/**
 * AKM Semiconductor, Inc.
 *  
 * Custom board for the AKDP Rev.D7 and Rev.E
 * 
 * Created by: Taylor Street
 * Date: 08-01-2018
 */

#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

#ifdef TARGET_NRF52_REVE

// PIN DEFINITIONS FOR AKDP REV.E
// AKDP Rev.E - Analog Pin Definitions
#define AIN0_PIN				2
#define AIN1_PIN				3
#define AIN2_PIN				5
#define ID1_PIN					AIN0_PIN
#define ID2_PIN					AIN1_PIN
#define BDETECT_PIN				AIN2_PIN

// AKDP Rev.E - Digital Pin Definitions
#define D0_PIN					28
#define D1_PIN					29
#define D2_PIN					30
#define D3_PIN					31
#define D4_PIN					8
#define D5_PIN					11
#define D6_PIN					12
#define D7_PIN					13
#define D8_PIN					17
#define D9_PIN					19
#define D10_PIN					9
#define D11_PIN					10
#define D12_PIN					14
#define D13_PIN					15
#define D14_PIN					16
#define D15_PIN					20
#define NRST_PIN				21

// AKDP Rev.E - LED Pin Definitions
#define LEDS_NUMBER				4
#define LED_START				22
#define LED_1					22
#define LED_2					25
#define LED_3					26
#define LED_4					27
#define LED_STOP				27
#define LEDS_ACTIVE_STATE		1
#define LEDS_INV_MASK			LEDS_MASK
#define LEDS_LIST				{ LED_1, LED_2, LED_3, LED_4 }
#define BSP_LED_0     			LED_1
#define BSP_LED_1				LED_2
#define BSP_LED_2				LED_3
#define BSP_LED_3				LED_4

// AKDP Rev.E - Button Pin Definitions
#define BUTTONS_NUMBER			1
#define BUTTON_START			21
#define BUTTON_1				21
#define BUTTON_STOP				21
#define BUTTON_PULL				NRF_GPIO_PIN_NOPULL
#define BUTTONS_ACTIVE_STATE	0
#define BUTTONS_LIST			{ BUTTON_1 }
#define BSP_BUTTON_0			BUTTON_1

// AKDP Rev.E - UART Serial Pin Definitions
#define RX_PIN_NUMBER			6			/**< UART Receive Pin Number. */
#define TX_PIN_NUMBER			7			/**< UART Transmit Pin Number. */
#define CTS_PIN_NUMBER			D13_PIN		/**< UART Clear-to-Send Pin Number. */
#define RTS_PIN_NUMBER			D14_PIN		/**< UART Ready-to-Send Pin Number. */
#define HWFC					false		/**< Hardware Flow Control. */

// AKDP Rev.E - SPI Pin Definitions
#define SPI_MOSI_PIN			D2_PIN		/**<  SPI MOSI signal. */
#define SPI_MISO_PIN			D1_PIN		/**<  SPI MISO signal. */
#define SPI_CSN_PIN				D0_PIN		/**<  SPI CSN signal. */
#define SPI_SCK_PIN				D3_PIN		/**<  SPI SCK signal. */
#define SPI_INT_PIN				D4_PIN		/**<  SPI interrupt pin. */

// AKDP Rev.E - I2C Pin Definitions
#define I2C_SCL_PIN				D3_PIN		/**<  I2C clock pin. */
#define I2C_SDA_PIN				D2_PIN		/**<  I2C data pin. */
#define I2C_INT_PIN				D0_PIN		/**<  I2C interrupt pin. */

#endif // TARGET_NRF52_REVE


#if defined(TARGET_NRF52_DK) && !defined(TARGET_NRF52_REVE) // TARGET_NRF52_DK

// PIN DEFINITIONS FOR AKDP REV.D7 V2
// AKDP Rev.D7 V2 - Analog Pin Definitions
//#define AIN0_PIN		
//#define AIN1_PIN		
//#define ID1_PIN			AIN0_PIN
//#define ID2_PIN			AIN1_PIN

// AKDP Rev.D7 V2 - Digital Pin Definitions
#define D0_PIN					30
#define D1_PIN					29
#define D2_PIN					28
#define D3_PIN					2
#define NRST_PIN				21

// AKDP Rev.D7 V2 - LED Pin Definitions
#define LED_BLE_NANO			11
#define LEDS_NUMBER				4
#define LED_START				D0_PIN
#define LED_1					D0_PIN
#define LED_2					D1_PIN
#define LED_3					D2_PIN
#define LED_4					D3_PIN
#define LED_STOP				D3_PIN
#define LEDS_ACTIVE_STATE		1
#define LEDS_INV_MASK  			LEDS_MASK
#define LEDS_LIST				{ LED_1, LED_2, LED_3, LED_4 }
#define BSP_LED_0      			LED_1
#define BSP_LED_1      			LED_2
#define BSP_LED_2      			LED_3
#define BSP_LED_3      			LED_4

// AKDP Rev.D7 V2 - Button Pin Definitions
#define BUTTONS_NUMBER 			1
#define BUTTON_START   			26
#define BUTTON_1       			26
#define BUTTON_STOP    			26
#define BUTTON_PULL    			NRF_GPIO_PIN_NOPULL
#define BUTTONS_ACTIVE_STATE	0
#define BUTTONS_LIST 			{ BUTTON_1 }
#define BSP_BUTTON_0			BUTTON_1

// AKDP Rev.D7 V2 - UART Serial Pin Definitions
#define RX_PIN_NUMBER			4			/**< UART Receive Pin Number. */
#define TX_PIN_NUMBER			5			/**< UART Transmit Pin Number. */
#define CTS_PIN_NUMBER			D2_PIN		/**< UART Clear-to-Send Pin Number. */
#define RTS_PIN_NUMBER			D3_PIN		/**< UART Ready-to-Send Pin Number. */
#define HWFC					false		/**< Hardware Flow Control. */

// AKDP Rev.D7 V2 - SPI Pin Definitions
#define SPI_MOSI_PIN			D2_PIN		/**<  SPI MOSI signal. */
#define SPI_MISO_PIN			D1_PIN		/**<  SPI MISO signal. */
#define SPI_CSN_PIN				D0_PIN		/**<  SPI CSN signal. */
#define SPI_SCK_PIN				D3_PIN		/**<  SPI SCK signal. */
//#define SPI_INT_PIN						/**<  SPI interrupt pin. */

// AKDP Rev.D7 V2 - I2C Pin Definitions
#define I2C_SCL_PIN				D3_PIN		/**<  I2C clock pin. */
#define I2C_SDA_PIN				D2_PIN		/**<  I2C data pin. */
#define I2C_INT_PIN				D0_PIN		/**<  I2C interrupt pin. */

#endif // TARGET_NRF52_DK


// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source       = NRF_CLOCK_LF_SRC_XTAL,      \
                                 .rc_ctiv      = 0,                          \
                                 .rc_temp_ctiv = 0,                          \
                                 .accuracy     = NRF_CLOCK_LF_ACCURACY_20_PPM}

#ifdef __cplusplus
}
#endif

#endif // CUSTOM_BOARD_H
